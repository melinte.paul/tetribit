#include <stdio.h>
#include <math.h>

//Melinte Paul-Eduard
//Grupa 312CD

#define RIGHT 0x0101010101010101
#define LEFT 0x8080808080808080
#define LINE 0x00000000000000ff

void print_map(unsigned long long map)
{
	int line,col;

	for(line=0;line<8;line++)
	{
		for(col=0;col<8;col++)
		{
			if(map & (1LL<<(63-col-8*line)))
				{
					printf("#");
				}
			else 
				{
					printf(".");
				}
		}
		printf("\n");
	}
	printf("\n");
}

void print_final_score(unsigned long long map, int completed_lines)
{
	int count=0,i;

	for(i=0;i<64;i++)
	{
		if(!(map & 1LL<<i)) 
		{
			count++;
		}
	}
	
	printf("GAME OVER!\n");
	printf("Score:%.2f\n", sqrt(count) + pow(1.25,completed_lines));
}

int check_coll(unsigned long long map, unsigned long long piece)
{	
	return (map & piece) !=0;
}

unsigned long long delete_line(unsigned long long map, int i)
{
	if(i == 0)
	{
		return map;
	}
	if(i == 8)
	{
		return map % 0x0100000000000000;
	}
	return (map % (1LL<<(8*(i-1))) + ((map / (1LL<<(8*i))<<(8*(i-1)))));	
}

int check_full_line(unsigned long long map)
{
	unsigned long long line = LINE;
 	int i;

 	for(i=0;i<8;i++)
 	{	
 		if((map & (line<<(8*i))) == (line<<(8*i)))
 		{
 			return i+1;
 		}
 	}
 	return 0;
}

int main()
{
	unsigned long long map,piece,init_piece;
	int completed_lines=0,completed_line;
	int i,j,n,deleted;
	int move[9];

	scanf("%llu%d",&map,&n);

	print_map(map);

	for(i=1;i<=n;i++)
	{
		scanf("%llu",&init_piece);

		for(j=1;j<=8;j++) 
		{
			scanf("%d",&move[j]);
		}
		
		piece = (init_piece & LINE) << 56;

		if(check_coll(map,piece))
		{
			print_map(map);
			break;
		}
		//caz final joc

		if(move[1] > 0)
		{	while (move[1] != 0)
			{
				move[1]--;
				if(!check_coll(map,piece>>1) && !check_coll(RIGHT,init_piece))
				{
					piece = piece >> 1;
					init_piece = init_piece >> 1;
				}
				else
				{
					move[1]=0;
				}
			}
		}
		else if(move[1] < 0)
		{
			while (move[1] != 0)
			{
				move[1]++;
				if(!check_coll(map,piece<<1) && !check_coll(LEFT,init_piece))
				{
					piece = piece << 1;
					init_piece = init_piece << 1;
				}
				else 
				{
					move[1]=0;
				}
			}
		}

		print_map(map + piece);

		if(check_coll(map,piece>>8))
		{	
			map = map + piece;
			if(init_piece & 0x000000000000ff00)
			{
				print_map(map);
				break;
			}
			else
			{
				continue;
			}
		}
		//caz final joc

		piece = init_piece << 48;

		for(j=2;j<=8;j++)
		{
			if(move[j] > 0)
			{
				while(move[j] != 0)
				{
					move[j]--;
					if(!check_coll(map | LEFT,piece>>1)) 
					{
						piece = piece >> 1;
					}
					else 
					{
						move[j]=0;
					}
				}
			}
			else if(move[j] < 0)
			{
				while(move[j] != 0)
				{
					move[j]++;
					if(!check_coll(map | RIGHT,piece<<1)) 
					{
						piece = piece << 1;
					}
					else 
					{
						move[j]=0;
					}
				}
			}

			print_map(map + piece);

			if(check_coll(map,piece>>8))
			{
				break;
			}
			else if(j!=8)
			{
				piece = piece >> 8;
			}
		}

		map = map + piece;

		deleted = 0;

		do{
			completed_line = check_full_line(map);
						if(completed_line != 0)
			{
				deleted++;
				map = delete_line(map,completed_line);
			}
		}while(completed_line);

		if(deleted)
		{
			print_map(map);
			completed_lines = completed_lines + deleted;
		}
	}

	print_final_score(map,completed_lines);

	return 0;
}